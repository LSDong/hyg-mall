-- 创建库
create database if not exists hygmall charset 'UTF8';

-- 优惠券模板表
CREATE TABLE `t_coupon_template`  (
`id` int(11) AUTO_INCREMENT,
`flag` int(11) COMMENT '状态：41.未审核 42.审核通过 43.审核失败',
`name` varchar(64) COMMENT '名字',
`logo` varchar(256) ,
`intro` varchar(256) COMMENT '简介',
`category` int(11) COMMENT '种类: 51-满减；52-折扣；53-立减',
`scope` int(11) COMMENT '使用范围：61-单品；62-商品类型；63-全品',
`scope_id` int(11) COMMENT '对应的id：单品id；商品类型id；全品为0',
`expire_time` datetime COMMENT '优惠券发放结束日期',
`coupon_count` int(11) COMMENT '优惠券发放数量',
`create_time` datetime COMMENT '创建时间',
`user_id` int(11) COMMENT '创建人的ID，后台内部员工',
`user_audit` varchar(100) COMMENT '审核意见',
`template_key` varchar(128) COMMENT '优惠券模板的识别码(有一定的识别度)',
`target` int(11) COMMENT '优惠券作用的人群：71-全体；72-会员等级 73-新用户 74-收费会员',
`target_level` int(11) COMMENT '用户等级要求，默认0',
`send_type` int comment '发放类型：81.用户领取 82.系统发放',
`start_time` datetime comment '优惠券生效日期',
`end_time` datetime comment '优惠券失效日期',
`limitmoney` decimal(10, 2) comment '优惠券可以使用的金额，满减、满折等',
`discount` double comment '减免或折扣' ,
PRIMARY KEY (`id`)
) comment '8.优惠券模板表';

-- 用户优惠券表
CREATE TABLE `t_usercoupon`  (
`id` int(11) AUTO_INCREMENT,
`template_id` int(11) COMMENT '优惠券模板ID',
`user_id` int(11) COMMENT '前端用户ID',
`coupon_code` varchar(70) COMMENT '优惠券码',
`assign_date` datetime COMMENT '优惠券分发时间',
`status` int(11) COMMENT '优惠券状态',
PRIMARY KEY (`id`)
) comment '9.用户优惠券表';