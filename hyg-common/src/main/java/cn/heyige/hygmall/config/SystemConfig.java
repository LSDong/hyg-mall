package cn.heyige.hygmall.config;

/**
 * 系统配置的字典类[常量类]
 *
 * @author Administrator
 * @date 2023/07/03
 */
public interface SystemConfig {

    // 未使用的优惠券
    Integer USER_COUPON_NO=91;

    // 已使用的优惠券
    Integer USER_COUPON_USED=92;

    // 已过期的优惠券
    Integer USER_COUPON_EXPIRED=93;

    // 自定义uid的消息头
    String HEADER_UID="cp_uid";

    // 优惠券的发送类型 81：用户优惠券 82：系统优惠券
    int COUPON_SEND_USER=81;
    int COUPON_SEND_SYSTEM=82;

    // 优惠券作用的人群：71-全体；72-会员等级 73-新用户 74-收费会员
    int COUPON_TARGET_ALL=71;
    int COUPON_TARGET_LEVEL=72;
    int COUPON_TARGET_NEW=73;
    int COUPON_TARGET_PLUS=74;

    // 批量新增用户优惠券数据的数量  线程池的任务处理的数量
    int THREAD_COUPON_BATCH=1000;
}
