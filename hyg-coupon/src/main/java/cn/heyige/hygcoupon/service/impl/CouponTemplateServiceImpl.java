package cn.heyige.hygcoupon.service.impl;

import cn.heyige.hygcoupon.couponenum.CouponAudit;
import cn.heyige.hygcoupon.dao.TCouponTemplateDao;
import cn.heyige.hygcoupon.dao.TUsercouponDao;
import cn.heyige.hygcoupon.entity.TCouponTemplate;
import cn.heyige.hygcoupon.entity.TUsercoupon;
import cn.heyige.hygcoupon.service.CouponTemplateService;
import cn.heyige.hygmall.config.SystemConfig;
import cn.heyige.hygmall.entity.dto.CouponAddDto;
import cn.heyige.hygmall.entity.dto.CouponAuditDto;
import cn.heyige.hygmall.utils.SnowFlowUtil;
import cn.heyige.hygmall.utils.ThreadPoolSignle;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(rollbackFor = Exception.class)
public class CouponTemplateServiceImpl implements CouponTemplateService {

    final TCouponTemplateDao couponTemplateDao;

    final TUsercouponDao userCouponDao;
    @Override
    public Boolean save(CouponAddDto dto) {
        //补全实体类数据
        TCouponTemplate template = new TCouponTemplate();
        //属性拷贝
        BeanUtils.copyProperties(dto,template);
        //补全其他属性
        template.setFlag(CouponAudit.未审核.getCode());
        template.setCreateTime(new Date());
        //优惠券模板的唯一标识  可以使用雪花算法生成
        long templateKey = SnowFlowUtil.getInstance().nextId();
        template.setTemplateKey(templateKey+"");
        return couponTemplateDao.insert(template)>0;

    }

    @Override
    public String audit(CouponAuditDto auditDto) {
        //获得优惠卷模板对象
        TCouponTemplate template = couponTemplateDao.queryById(auditDto.getId());
        //判断模板状态
        if (Objects.nonNull(template) && template.getFlag().equals(CouponAudit.未审核.getCode())){
            //进行审核，更新优惠卷模块
            //审核状态
            template.setFlag(auditDto.getFlag());
            //审核意见
            template.setUserAudit(auditDto.getInfo());
            //审核人
            template.setUserId(auditDto.getAid());
            if (couponTemplateDao.update(template)>0){
                //状态更新成功
                if (auditDto.getFlag().equals(CouponAudit.审核通过.getCode())){
                    //审核通过就要发放优惠卷了
                    List<Integer> userIds = getLeveUser();
                    Integer templateId = template.getId();
                    String templateKey = template.getTemplateKey();
//                    //创建用户优惠卷实体对象的集合
//                    List<TUsercoupon> list = userIds.stream().map(uid -> new TUsercoupon(templateId, uid, templateKey)).collect(Collectors.toList());
//                    userCouponDao.insertBatch(list);
                    //计算分多少批进行处理
                    int batch = userIds.size() / SystemConfig.THREAD_COUPON_BATCH;
                    batch = userIds.size()%SystemConfig.THREAD_COUPON_BATCH == 0?batch:batch+1;
                    //如果数据量太大直接操作数据库，优化为使用线程池并发处理
                    for (int i=0;i<batch;i++){
                        //每个线程处理数据的初始值
                        int begin=i * SystemConfig.THREAD_COUPON_BATCH;
                        ThreadPoolSignle.getInstance().poolExecutor.execute(()->{
                         //每个线程池并发处理
                         //创建用户优惠卷实体对象的集合
                            System.out.println("线程"+Thread.currentThread().getName()+"从第"+begin+"条数据开始处理");
                            List<TUsercoupon> list = userIds.stream().skip(begin).limit(SystemConfig.THREAD_COUPON_BATCH).map(uid -> new TUsercoupon(templateId, uid, templateKey)).collect(Collectors.toList());
                            userCouponDao.insertBatch(list);
                        });


                    }
                    return "审核通过";

                }else {
                    return "审核拒绝";
                }
            }
        }else {
            return "亲，非法操作";
        }
        return "审核失败";
    }

    @Override
    public PageInfo<TCouponTemplate> findByPage(Integer page, Integer size) {
        PageHelper.startPage(page,size);
        List<TCouponTemplate> list = couponTemplateDao.listAll();

        return new PageInfo<>(list);
    }

    @Override
    public TCouponTemplate findById(Integer id) {
        return couponTemplateDao.queryById(id);
    }


    //获取某个等级的用户ID集合
    private List<Integer> getLeveUser(){
        return Stream.iterate(1,id->id+1).limit(20005).collect(Collectors.toList());
    }
}
