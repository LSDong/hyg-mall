package cn.heyige.hygcoupon.service;

import cn.heyige.hygcoupon.entity.TCouponTemplate;
import cn.heyige.hygmall.entity.dto.CouponAddDto;
import cn.heyige.hygmall.entity.dto.CouponAuditDto;
import com.github.pagehelper.PageInfo;

public interface CouponTemplateService {

    //新增优惠卷模板
    Boolean save(CouponAddDto dto);

    //审核优惠卷
    String audit(CouponAuditDto auditDto);

    //查看模板列表
    PageInfo<TCouponTemplate> findByPage(Integer page,Integer size);


    //查看模板详情
    TCouponTemplate findById(Integer id);

}
