package cn.heyige.hygcoupon.controller;

import cn.heyige.hygcoupon.entity.TCouponTemplate;
import cn.heyige.hygcoupon.service.CouponTemplateService;
import cn.heyige.hygmall.entity.R;
import cn.heyige.hygmall.entity.dto.CouponAddDto;
import cn.heyige.hygmall.entity.dto.CouponAuditDto;
import com.github.pagehelper.PageInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("coupon/template")
@RequiredArgsConstructor
public class CouponTemplateController {

    final CouponTemplateService couponTemplateService;

    @PostMapping("save")
    public R<String> save(CouponAddDto dto){
        //进行参数校验
        if (couponTemplateService.save(dto)){
            return R.ok("新增优惠卷模板成功");
        }
        return R.fail("新增优惠卷模板失败");
    }


    @PostMapping("page")
    public R findByPage(@RequestParam(value = "page",defaultValue = "1",required = false)Integer page,
                        @RequestParam(value = "size",defaultValue = "10",required = false)Integer size){
        PageInfo<TCouponTemplate> pageInfo = couponTemplateService.findByPage(page, size);
        return R.ok(pageInfo);
    }

    @GetMapping("{id}")
    public R<TCouponTemplate> findById(@PathVariable Integer id){
        TCouponTemplate template = couponTemplateService.findById(id);
        return R.ok(template);
    }

    //审核优惠卷活动
    @PostMapping("audit")
    public R<String> audit(@RequestBody CouponAuditDto dto){
        String audit = couponTemplateService.audit(dto);
        return R.ok(audit);
    }
}
