package cn.heyige.hygcoupon.dao;

import cn.heyige.hygcoupon.entity.TUsercoupon;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (TUsercoupon)表数据库访问层
 *
 * @author mudong
 * @since 2023-07-03 17:40:45
 */
public interface TUsercouponDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    TUsercoupon queryById(Integer id);

    /**
     * 列出所有
     *
     * @return 实例集合
     */
    List<TUsercoupon> listAll();

    /**
     * 统计总行数
     *
     * @param tUsercoupon 查询条件
     * @return 总行数
     */
    long count(TUsercoupon tUsercoupon);

    /**
     * 新增数据
     *
     * @param tUsercoupon 实例对象
     * @return 影响行数
     */
    int insert(TUsercoupon tUsercoupon);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<TUsercoupon> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<TUsercoupon> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<TUsercoupon> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<TUsercoupon> entities);

    /**
     * 修改数据
     *
     * @param tUsercoupon 实例对象
     * @return 影响行数
     */
    int update(TUsercoupon tUsercoupon);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

}

