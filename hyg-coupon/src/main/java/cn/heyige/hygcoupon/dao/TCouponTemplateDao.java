package cn.heyige.hygcoupon.dao;

import cn.heyige.hygcoupon.entity.TCouponTemplate;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 8.优惠券模板表(TCouponTemplate)表数据库访问层
 *
 * @author mudong
 * @since 2023-07-03 11:58:01
 */
public interface TCouponTemplateDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    TCouponTemplate queryById(Integer id);

    /**
     * 列出所有
     *
     * @return 实例集合
     */
    List<TCouponTemplate> listAll();

    /**
     * 统计总行数
     *
     * @param tCouponTemplate 查询条件
     * @return 总行数
     */
    long count(TCouponTemplate tCouponTemplate);

    /**
     * 新增数据
     *
     * @param tCouponTemplate 实例对象
     * @return 影响行数
     */
    int insert(TCouponTemplate tCouponTemplate);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<TCouponTemplate> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<TCouponTemplate> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<TCouponTemplate> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<TCouponTemplate> entities);

    /**
     * 修改数据
     *
     * @param tCouponTemplate 实例对象
     * @return 影响行数
     */
    int update(TCouponTemplate tCouponTemplate);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

}

