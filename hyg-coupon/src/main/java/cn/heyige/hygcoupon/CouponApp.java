package cn.heyige.hygcoupon;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan(basePackages = "cn.heyige.hygcoupon.dao")
public class CouponApp {
    public static void main(String[] args) {
        SpringApplication.run(CouponApp.class,args);
    }
}
